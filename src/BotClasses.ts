﻿export namespace AdBoxBot {

    export interface VisitorItem {
        time: number;
        userId: string;    
    };

    export interface InlineItem {
        chatId: string;
        orders: Array<VisitorItem>;
        visitors: Array<VisitorItem>;
        query: string;
        time: number;
        userId: string;
    }
    export interface AdBoxItem {
        photo: string;
        name: string;
        short: string;
        id: string;
        desc: string;
        button: string,
        button2: string,
        buy_url: string,
        hide_add: boolean,
        ownerId: string,
        inlineItems: Array<InlineItem>,
        cost: number
    }

    export interface AdBoxMessage {
        chatId: number;
        msg: any;
        args: Array<string>;
        answer?: string;
        command: string;
        session?: any;
        inline_message_id?: string;
        inline_query_id?: string;
        chat_instance?: string;
        botId: string;
    }

    export interface DelayedOrder {
        cost: number;
        itemId: string;
        inlineId: string;
        userId: string | number;
    }

    export interface AdBoxWalletLogEntry {
        change: number;
        message1: string;
        message2: string;
        time: number;
    }

    export interface AdBoxWallet {
        amount: number;
        logs: Array<AdBoxWalletLogEntry>;
    }

    export interface AdBoxCustomer {
        amo_domain?: string;
        amo_hash?: string;
        amo_login?: string;
        first_name?: string;
        last_name?: string;
        id?: string;
        is_bot?: boolean;
        language_code?: string;
        phone?: string;
        username?: string;
        lang: string;
        ads: Array<{ ownerId: string, itemId: string }>;
        wallet?: AdBoxWallet;
    }
}