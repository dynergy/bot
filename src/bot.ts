﻿import * as redis from './red';
import * as i18n from 'i18n';
import * as TelegramBot from 'node-telegram-bot-api';
import * as Tgfancy from "tgfancy";
import * as botanio2 from 'botanio';
import * as Promise2 from "bluebird";

//import * as moment from 'moment-timezone';
import moment2 = require('moment-timezone');

export interface BotInfo {
    botan: typeof botanio2,
    firebase: any,
    moment: typeof moment2,
    bot: TelegramBot,
    botName: string,
    i18n: typeof i18n
    
}

const RETRIABLE_ERRORS: Array<string> = ['ECONNRESET', 'ENOTFOUND', 'ESOCKETTIMEDOUT', 'ETIMEDOUT', 'ECONNREFUSED', 'EHOSTUNREACH', 'EPIPE', 'EAI_AGAIN', 'EPARSE'];

class DynergyTelegramBot extends Tgfancy {

    constructor(token, options = {}) {
        super(token, options);
        const self = this; 
        this.execWithRetriesFns.forEach(function (methodName) {
            self[methodName] = self.execWithTries(self[methodName]);
        }); 
    }

    
    readonly execWithRetriesFns: Array<string> = [
        "addStickerToSet",
        "answerCallbackQuery",
        "answerInlineQuery",
        "answerPreCheckoutQuery",
        "answerShippingQuery",
        "createNewStickerSet",
        "deleteChatPhoto",
        "deleteMessage",
        "deleteStickerFromSet",
        "downloadFile",
        "editMessageCaption",
        "editMessageReplyMarkup",
        "editMessageText",
        "exportChatInviteLink",
        "forwardMessage",
        "getChat",
        "getChatAdministrators",
        "getChatMember",
        "getChatMembersCount",
        "getFile",
        "getFileLink",
        "getGameHighScores",
        "getStickerSet",
        "getUpdates",
        "getUserProfilePhotos",
        "kickChatMember",
        "leaveChat",
        "pinChatMessage",
        "promoteChatMember",
        "restrictChatMember",
        "sendAudio",
        "sendChatAction",
        "sendContact",
        "sendDocument",
        "sendGame",
        "sendInvoice",
        "sendLocation",
        "sendMessage",
        "sendPhoto",
        "sendSticker",
        "sendVenue",
        "sendVideo",
        "sendVideoNote",
        "sendVoice",
        "setChatDescription",
        "setChatPhoto",
        "setChatTitle",
        "setGameScore",
        "setStickerPositionInSet",
        "setWebHook",
        "unbanChatMember",
        "unpinChatMessage",
        "uploadStickerFile",
    ];

    
    execWithTries(method) {
        const self = this;
        return function (...args) {
            return new Promise2(async function (resolve, reject) {            
                let retries = 20;
                var result;
                while (retries > 0) {
                    try {
                        result = await method.call(self, ...args);
                    }
                    catch (e) {
                        var httpCode, ref;
                        retries--;
                        httpCode = parseInt(e.message);
                        if (retries > 0 && ((ref = e != null ? e.code : void 0, RETRIABLE_ERRORS.includes(ref)) || (500 <= httpCode && httpCode < 600) || httpCode === 420)) {
                            continue;
                        }

                        console.log("Reject with ref " + ref + ", retries left: " + retries);
                        reject(result);
                        return;
                    }
                    
                    if (typeof result == typeof Error) {
                        var e = result;
                        var httpCode, ref;
                        retries--;
                        httpCode = parseInt(e.message);
                        if (retries > 0 && ((ref = e != null ? e.code : void 0, RETRIABLE_ERRORS.includes(ref)) || (500 <= httpCode && httpCode < 600) || httpCode === 420)) {
                            continue;
                        }
                        reject(result);
                        return;
                    }
                    resolve(result);
                    return;
                }
                reject(result);
            //return new Promise(function (resolve, reject) {
                //exec(resolve, reject);
            //})
            }); 
        }
    }

    /*
    execWithTries(method) {
        const self = this; 
        return function (...args) {
            let retries = 20;
            function exec(resolve, reject) {
                method.call(self, ...args)
                    .then(resolve)
                    .catch(function (e) {
                        var httpCode, ref;
                        httpCode = parseInt(e.message);
                        if (retries-- > 0 && ((ref = e != null ? e.code : void 0, self.RETRIABLE_ERRORS.includes(ref)) || (500 <= httpCode && httpCode < 600) || httpCode === 420)) {
                            exec(resolve, reject);
                        } else {
                            return reject(e);
                        }
                    });
            }

            return new Promise(function (resolve, reject) {
                exec(resolve, reject);
            })
        }
    }
    */
}

export var initFunction = async function (telegramBotKey: string, i18Dir: string, botanKey: string, firebase2, proxy = undefined) {

    //i18Dir = __dirname + ' /../../ locales';
    console.log('init locale at ' + i18Dir);
    i18n.configure({
        locales: ['en', 'ru'],
        fallbacks: {
            'ru-RU': 'ru',
            'en-US': 'en'
        },
        directory: i18Dir
    });

    const botan = botanio2(botanKey);

    moment2.tz.setDefault("Europe/Moscow");
    moment2.locale("ru");

    if (i18n.getCatalog('en')) { console.log('En locale correct'); }
    if (i18n.getCatalog('ru')) { console.log('Ru locale correct'); }
    console.log(i18n.__({ phrase: 'Hello2 %s', locale: 'ru' }, 'World'));

    let options = {
        polling: true,
        tgfancy: {
            feature: true,  // 'true' to enable!
        },
    }

    if (proxy) {
        options['request'] = {
            proxy: proxy,
        }
    };
    
    const bot: TelegramBot = <any>new DynergyTelegramBot(telegramBotKey, options);
    
    var result = {
        botan: botan,
        firebase: firebase2,
        moment: moment2,
        bot: bot,
        botName: '',
        i18n: i18n
    }

    var me: TelegramBot.User | Error = await bot.getMe();
    console.log(me);
    result.botName = (<TelegramBot.User>me).username;
    
    return result;
}

export default initFunction;

async function exec(self, method, retries, ...args) {
    
}



