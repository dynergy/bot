﻿import * as base2 from './baseController';
import { initFunction, BotInfo } from './bot'
import * as TelegramBot from 'node-telegram-bot-api';
import * as moment2 from 'moment-timezone';
export { AdBoxBot } from './BotClasses';


export let baseClass = base2.baseController;
export let init = initFunction;
export { BotInfo } from './bot'
export { commandsPack } from './baseController'
export { redisSave, reditGet } from './red';


