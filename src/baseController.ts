﻿import log from './log';
import * as brother from './bot';
import * as i18n from 'i18n';
import redis = require('./red');
import * as EventEmitter from 'pattern-emitter';

//Promise.resolve(1);
//var EventEmitter = require('event-emitter-extra');
//import { EventEmitterEx } from 'event-emitter-extra';

import * as botanio2 from 'botanio';
import * as BotClasses from './BotClasses';

import * as TelegramBot from 'node-telegram-bot-api';

export interface commandsPack {
    answers?: Array<{ name: string | RegExp, handler: Function }>;
    commands?: Array<{ name: string | RegExp, handler: Function }>;
}



export class baseController {
    readonly bot: TelegramBot;
    readonly firebase:any;
    readonly botan: botanio2;
    readonly moment;
    readonly commandsEmitter = new EventEmitter();
    readonly answersEmitter = new EventEmitter();
    readonly log = log;
    readonly i18n: typeof i18n;
    readonly botName: string;
    constructor(info: brother.BotInfo) {

        
        this.bot = info.bot;
        this.firebase = info.firebase;
        this.botan = info.botan;
        this.moment = info.moment;
        this.i18n = info.i18n;
        this.botName = info.botName;

        var commandsPack = this.getCommands();
        this.initCommands(true, commandsPack.commands);
        this.initCommands(false, commandsPack.answers);

        // Listen for any kind of message. There are different kinds of messages.
        var t = this;
        this.bot.on('message', function (msg: TelegramBot.Message) { t.onMessage(msg) });
        this.bot.on('callback_query', query => t.queryCallback(query));
    }

    initCommands(isCommand: boolean, commands: Array<{ name: string | RegExp, handler: Function }>) {

        if (!commands)
            return;
        var t2 = this;

        for (var i = 0; i < commands.length; i++) {
            let t = commands[i];

            let resolve = function (args) {
                return t.handler.apply(t2, [args]);
            }
            if (isCommand) {
                t2.setCommand(t.name, resolve);
            }
            else {
                t2.setAnswer(t.name, resolve);
            }
        }
    }

    getCommands(): commandsPack {
        return {}
    }

    send_inline_keyboard(chatId: string | number, message: string, buttons: TelegramBot.InlineKeyboardButton[][]) {
        var options: TelegramBot.SendMessageOptions = {
            reply_markup: {
                inline_keyboard: buttons
            }
        };

        return this.bot.sendMessage(chatId, message, options).catch(function (reason) {
            console.error(reason)
            return null;
        });
    }

    async onMessage(msg: TelegramBot.Message) {

        try {
            const chatId = msg.from.id;

            var command: string, args: Array<string> = [];
            if (msg.text) {
                var words = msg.text.split(' ');
                args = words.slice(1);

                if (msg.text[0] == '/') {
                    command = words[0].substring(1);

                }
            }


            var data = {
                chatId: chatId,
                msg: msg,
                args: args,
                command: command,
                botId: this.botName,
            }

            this.execEvent(data);
            this.botan.track(msg, command);
        }
        catch (e) {
            console.error("onMessage error:");
            console.error(e);
            throw e;
        }
    };

    async queryCallback(query: TelegramBot.CallbackQuery) {
        //this.log.info(query, 'callback_query')
        try {
            const chatId = query.from.id;
            const msg = query.message;
            var words: string[] = query.data.split('|');
            var command: string = words[0];
            var data: BotClasses.AdBoxBot.AdBoxMessage = {
                chatId: chatId,
                msg: msg,
                args: words.slice(1),
                command: command,
                inline_query_id: query.id,
                chat_instance: query.chat_instance,
                inline_message_id: query.inline_message_id,
                botId: this.botName

            }

            if (query.inline_message_id) {
                this.firebase.setChatInstance(query.inline_message_id, query.chat_instance);
            }
            this.botan.track(query, command);
            await this.execEvent(data);
            this.bot.answerCallbackQuery({ callback_query_id: query.id });
        }
        catch (error) {
            this.log.error('Error at queryCallback');
            this.log.error(error);
        }
    }
    

    async getSession(chatId) {

        var botId = this.botName;

        let session = await redis.reditGet(botId, chatId.toString());
        if (!session) {
            session = {};
        }

        if (!session.lang) {
            let customer = await this.firebase.getCustomer(chatId)
            if (customer) {
                session.lang = customer.lang || customer.language_code;
                if (!session.lang) {
                    session.lang = 'en';
                }
            }
        }
        return session;
    }

    async saveSession(botId, chatId, session) {

        await redis.redisSave(botId, chatId, session)
    }

    async execEvent(data: BotClasses.AdBoxBot.AdBoxMessage, overrideSession: boolean = true) {
        return new Promise(async (resolve, reject) => {

            //data = data || {};

            var chatId = data.chatId;

            var emitter: any = this.commandsEmitter;


            var session = data.session;

            if (overrideSession || !session) {
                session = await this.getSession(chatId)
            }
            data.session = session;

            let command: string;
            if (data.command) {
                command = data.command;

            }
            else {
                emitter = this.answersEmitter;
                command = session.command;

                data.answer = data.msg.text;
                data.args = [];

                if (!command) {
                    command = 'start';
                }
                data.command = command;
            }
            delete data.session.command;
            await this.before(data);
            command = data.command;

            await this.emitterEmit(emitter, command, data);

            data.session.command = data.command;

            var botId = data.botId;

            this.saveSession(botId, chatId.toString(), data.session);

            resolve();

        })
    }
    

    changeCommand(name: string, data: any) {
        data.command = name;
    }

    gotoCommand(name: string, data: BotClasses.AdBoxBot.AdBoxMessage) {
        data.command = name;
        return this.execEvent(data, false);
    }

    setCommand(name: string | RegExp, handler: Function) {
        this.setInvoke(this.commandsEmitter, name, handler);
    }

    setAnswer(name: string | RegExp, handler: Function) {
        this.setInvoke(this.answersEmitter, name, handler);
    }

    setInvoke(emitter, name: string | RegExp, handler: Function) {

        emitter.on(name, async function (data: any, callback: Function) {
            await handler(data);
            callback();
        });
    }

    async before(data) {
        return new Promise(async (resolve, reject) => {
            resolve();
        });
    }

    async emitterEmit(emitter, command, data) {
        return new Promise(async (resolve, reject) => {

            emitter.emit(command, data, function () {
                resolve();
            })
        });
    }
}