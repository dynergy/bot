﻿import red = require("redis");
import log from './log';

var redisUrl = process.env.REDIS_URL;

var redisClient: red.RedisClient = red.createClient(redisUrl);

redisClient.on("error", function (err) {
    log.error("Error " + err);
});

export async function redisSave(botId: string, id:string, session:any) {
    return new Promise(resolve => {
        redisClient.hset("BOTSESSIONS:" + botId, id, JSON.stringify(session), (err, reply) => {

            resolve();
        });
    });
    
};

export function reditGet(botId: string, id: string): Promise<any> {
    return new Promise(resolve => {
        redisClient.hget("BOTSESSIONS:" + botId, id, (err, session) => {

            session = session && JSON.parse(session);
            resolve(session);
        })
    });

    
};