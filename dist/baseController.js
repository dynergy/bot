"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const log_1 = require("./log");
const redis = require("./red");
const EventEmitter = require("pattern-emitter");
class baseController {
    constructor(info) {
        this.commandsEmitter = new EventEmitter();
        this.answersEmitter = new EventEmitter();
        this.log = log_1.default;
        this.bot = info.bot;
        this.firebase = info.firebase;
        this.botan = info.botan;
        this.moment = info.moment;
        this.i18n = info.i18n;
        this.botName = info.botName;
        var commandsPack = this.getCommands();
        this.initCommands(true, commandsPack.commands);
        this.initCommands(false, commandsPack.answers);
        var t = this;
        this.bot.on('message', function (msg) { t.onMessage(msg); });
        this.bot.on('callback_query', query => t.queryCallback(query));
    }
    initCommands(isCommand, commands) {
        if (!commands)
            return;
        var t2 = this;
        for (var i = 0; i < commands.length; i++) {
            let t = commands[i];
            let resolve = function (args) {
                return t.handler.apply(t2, [args]);
            };
            if (isCommand) {
                t2.setCommand(t.name, resolve);
            }
            else {
                t2.setAnswer(t.name, resolve);
            }
        }
    }
    getCommands() {
        return {};
    }
    send_inline_keyboard(chatId, message, buttons) {
        var options = {
            reply_markup: {
                inline_keyboard: buttons
            }
        };
        return this.bot.sendMessage(chatId, message, options).catch(function (reason) {
            console.error(reason);
            return null;
        });
    }
    onMessage(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chatId = msg.from.id;
                var command, args = [];
                if (msg.text) {
                    var words = msg.text.split(' ');
                    args = words.slice(1);
                    if (msg.text[0] == '/') {
                        command = words[0].substring(1);
                    }
                }
                var data = {
                    chatId: chatId,
                    msg: msg,
                    args: args,
                    command: command,
                    botId: this.botName,
                };
                this.execEvent(data);
                this.botan.track(msg, command);
            }
            catch (e) {
                console.error("onMessage error:");
                console.error(e);
                throw e;
            }
        });
    }
    ;
    queryCallback(query) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const chatId = query.from.id;
                const msg = query.message;
                var words = query.data.split('|');
                var command = words[0];
                var data = {
                    chatId: chatId,
                    msg: msg,
                    args: words.slice(1),
                    command: command,
                    inline_query_id: query.id,
                    chat_instance: query.chat_instance,
                    inline_message_id: query.inline_message_id,
                    botId: this.botName
                };
                if (query.inline_message_id) {
                    this.firebase.setChatInstance(query.inline_message_id, query.chat_instance);
                }
                this.botan.track(query, command);
                yield this.execEvent(data);
                this.bot.answerCallbackQuery({ callback_query_id: query.id });
            }
            catch (error) {
                this.log.error('Error at queryCallback');
                this.log.error(error);
            }
        });
    }
    getSession(chatId) {
        return __awaiter(this, void 0, void 0, function* () {
            var botId = this.botName;
            let session = yield redis.reditGet(botId, chatId.toString());
            if (!session) {
                session = {};
            }
            if (!session.lang) {
                let customer = yield this.firebase.getCustomer(chatId);
                if (customer) {
                    session.lang = customer.lang || customer.language_code;
                    if (!session.lang) {
                        session.lang = 'en';
                    }
                }
            }
            return session;
        });
    }
    saveSession(botId, chatId, session) {
        return __awaiter(this, void 0, void 0, function* () {
            yield redis.redisSave(botId, chatId, session);
        });
    }
    execEvent(data, overrideSession = true) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                var chatId = data.chatId;
                var emitter = this.commandsEmitter;
                var session = data.session;
                if (overrideSession || !session) {
                    session = yield this.getSession(chatId);
                }
                data.session = session;
                let command;
                if (data.command) {
                    command = data.command;
                }
                else {
                    emitter = this.answersEmitter;
                    command = session.command;
                    data.answer = data.msg.text;
                    data.args = [];
                    if (!command) {
                        command = 'start';
                    }
                    data.command = command;
                }
                delete data.session.command;
                yield this.before(data);
                command = data.command;
                yield this.emitterEmit(emitter, command, data);
                data.session.command = data.command;
                var botId = data.botId;
                this.saveSession(botId, chatId.toString(), data.session);
                resolve();
            }));
        });
    }
    changeCommand(name, data) {
        data.command = name;
    }
    gotoCommand(name, data) {
        data.command = name;
        return this.execEvent(data, false);
    }
    setCommand(name, handler) {
        this.setInvoke(this.commandsEmitter, name, handler);
    }
    setAnswer(name, handler) {
        this.setInvoke(this.answersEmitter, name, handler);
    }
    setInvoke(emitter, name, handler) {
        emitter.on(name, function (data, callback) {
            return __awaiter(this, void 0, void 0, function* () {
                yield handler(data);
                callback();
            });
        });
    }
    before(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                resolve();
            }));
        });
    }
    emitterEmit(emitter, command, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                emitter.emit(command, data, function () {
                    resolve();
                });
            }));
        });
    }
}
exports.baseController = baseController;
//# sourceMappingURL=baseController.js.map