"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const i18n = require("i18n");
const Tgfancy = require("tgfancy");
const botanio2 = require("botanio");
const Promise2 = require("bluebird");
const moment2 = require("moment-timezone");
const RETRIABLE_ERRORS = ['ECONNRESET', 'ENOTFOUND', 'ESOCKETTIMEDOUT', 'ETIMEDOUT', 'ECONNREFUSED', 'EHOSTUNREACH', 'EPIPE', 'EAI_AGAIN', 'EPARSE'];
class DynergyTelegramBot extends Tgfancy {
    constructor(token, options = {}) {
        super(token, options);
        this.execWithRetriesFns = [
            "addStickerToSet",
            "answerCallbackQuery",
            "answerInlineQuery",
            "answerPreCheckoutQuery",
            "answerShippingQuery",
            "createNewStickerSet",
            "deleteChatPhoto",
            "deleteMessage",
            "deleteStickerFromSet",
            "downloadFile",
            "editMessageCaption",
            "editMessageReplyMarkup",
            "editMessageText",
            "exportChatInviteLink",
            "forwardMessage",
            "getChat",
            "getChatAdministrators",
            "getChatMember",
            "getChatMembersCount",
            "getFile",
            "getFileLink",
            "getGameHighScores",
            "getStickerSet",
            "getUpdates",
            "getUserProfilePhotos",
            "kickChatMember",
            "leaveChat",
            "pinChatMessage",
            "promoteChatMember",
            "restrictChatMember",
            "sendAudio",
            "sendChatAction",
            "sendContact",
            "sendDocument",
            "sendGame",
            "sendInvoice",
            "sendLocation",
            "sendMessage",
            "sendPhoto",
            "sendSticker",
            "sendVenue",
            "sendVideo",
            "sendVideoNote",
            "sendVoice",
            "setChatDescription",
            "setChatPhoto",
            "setChatTitle",
            "setGameScore",
            "setStickerPositionInSet",
            "setWebHook",
            "unbanChatMember",
            "unpinChatMessage",
            "uploadStickerFile",
        ];
        const self = this;
        this.execWithRetriesFns.forEach(function (methodName) {
            self[methodName] = self.execWithTries(self[methodName]);
        });
    }
    execWithTries(method) {
        const self = this;
        return function (...args) {
            return new Promise2(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    let retries = 20;
                    var result;
                    while (retries > 0) {
                        try {
                            result = yield method.call(self, ...args);
                        }
                        catch (e) {
                            var httpCode, ref;
                            retries--;
                            httpCode = parseInt(e.message);
                            if (retries > 0 && ((ref = e != null ? e.code : void 0, RETRIABLE_ERRORS.includes(ref)) || (500 <= httpCode && httpCode < 600) || httpCode === 420)) {
                                continue;
                            }
                            console.log("Reject with ref " + ref + ", retries left: " + retries);
                            reject(result);
                            return;
                        }
                        if (typeof result == typeof Error) {
                            var e = result;
                            var httpCode, ref;
                            retries--;
                            httpCode = parseInt(e.message);
                            if (retries > 0 && ((ref = e != null ? e.code : void 0, RETRIABLE_ERRORS.includes(ref)) || (500 <= httpCode && httpCode < 600) || httpCode === 420)) {
                                continue;
                            }
                            reject(result);
                            return;
                        }
                        resolve(result);
                        return;
                    }
                    reject(result);
                });
            });
        };
    }
}
exports.initFunction = function (telegramBotKey, i18Dir, botanKey, firebase2, proxy = undefined) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('init locale at ' + i18Dir);
        i18n.configure({
            locales: ['en', 'ru'],
            fallbacks: {
                'ru-RU': 'ru',
                'en-US': 'en'
            },
            directory: i18Dir
        });
        const botan = botanio2(botanKey);
        moment2.tz.setDefault("Europe/Moscow");
        moment2.locale("ru");
        if (i18n.getCatalog('en')) {
            console.log('En locale correct');
        }
        if (i18n.getCatalog('ru')) {
            console.log('Ru locale correct');
        }
        console.log(i18n.__({ phrase: 'Hello2 %s', locale: 'ru' }, 'World'));
        let options = {
            polling: true,
            tgfancy: {
                feature: true,
            },
        };
        if (proxy) {
            options['request'] = {
                proxy: proxy,
            };
        }
        ;
        const bot = new DynergyTelegramBot(telegramBotKey, options);
        var result = {
            botan: botan,
            firebase: firebase2,
            moment: moment2,
            bot: bot,
            botName: '',
            i18n: i18n
        };
        var me = yield bot.getMe();
        console.log(me);
        result.botName = me.username;
        return result;
    });
};
exports.default = exports.initFunction;
function exec(self, method, retries, ...args) {
    return __awaiter(this, void 0, void 0, function* () {
    });
}
//# sourceMappingURL=bot.js.map