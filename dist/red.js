"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const red = require("redis");
const log_1 = require("./log");
var redisUrl = process.env.REDIS_URL;
var redisClient = red.createClient(redisUrl);
redisClient.on("error", function (err) {
    log_1.default.error("Error " + err);
});
function redisSave(botId, id, session) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            redisClient.hset("BOTSESSIONS:" + botId, id, JSON.stringify(session), (err, reply) => {
                resolve();
            });
        });
    });
}
exports.redisSave = redisSave;
;
function reditGet(botId, id) {
    return new Promise(resolve => {
        redisClient.hget("BOTSESSIONS:" + botId, id, (err, session) => {
            session = session && JSON.parse(session);
            resolve(session);
        });
    });
}
exports.reditGet = reditGet;
;
//# sourceMappingURL=red.js.map