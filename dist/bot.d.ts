/// <reference types="node-telegram-bot-api" />
/// <reference types="i18n" />
import * as i18n from 'i18n';
import * as TelegramBot from 'node-telegram-bot-api';
import * as botanio2 from 'botanio';
import moment2 = require('moment-timezone');
export interface BotInfo {
    botan: typeof botanio2;
    firebase: any;
    moment: typeof moment2;
    bot: TelegramBot;
    botName: string;
    i18n: typeof i18n;
}
export declare var initFunction: (telegramBotKey: string, i18Dir: string, botanKey: string, firebase2: any, proxy?: any) => Promise<{
    botan: any;
    firebase: any;
    moment: typeof moment2;
    bot: TelegramBot;
    botName: string;
    i18n: typeof i18n;
}>;
export default initFunction;
