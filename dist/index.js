"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base2 = require("./baseController");
const bot_1 = require("./bot");
var BotClasses_1 = require("./BotClasses");
exports.AdBoxBot = BotClasses_1.AdBoxBot;
exports.baseClass = base2.baseController;
exports.init = bot_1.initFunction;
var red_1 = require("./red");
exports.redisSave = red_1.redisSave;
exports.reditGet = red_1.reditGet;
//# sourceMappingURL=index.js.map