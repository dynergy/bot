/// <reference types="node-telegram-bot-api" />
/// <reference types="i18n" />
import * as brother from './bot';
import * as i18n from 'i18n';
import * as botanio2 from 'botanio';
import * as BotClasses from './BotClasses';
import * as TelegramBot from 'node-telegram-bot-api';
export interface commandsPack {
    answers?: Array<{
        name: string | RegExp;
        handler: Function;
    }>;
    commands?: Array<{
        name: string | RegExp;
        handler: Function;
    }>;
}
export declare class baseController {
    readonly bot: TelegramBot;
    readonly firebase: any;
    readonly botan: botanio2;
    readonly moment: any;
    readonly commandsEmitter: any;
    readonly answersEmitter: any;
    readonly log: {
        error: (str: any) => void;
    };
    readonly i18n: typeof i18n;
    readonly botName: string;
    constructor(info: brother.BotInfo);
    initCommands(isCommand: boolean, commands: Array<{
        name: string | RegExp;
        handler: Function;
    }>): void;
    getCommands(): commandsPack;
    send_inline_keyboard(chatId: string | number, message: string, buttons: TelegramBot.InlineKeyboardButton[][]): Promise<any>;
    onMessage(msg: TelegramBot.Message): Promise<void>;
    queryCallback(query: TelegramBot.CallbackQuery): Promise<void>;
    getSession(chatId: any): Promise<any>;
    saveSession(botId: any, chatId: any, session: any): Promise<void>;
    execEvent(data: BotClasses.AdBoxBot.AdBoxMessage, overrideSession?: boolean): Promise<{}>;
    changeCommand(name: string, data: any): void;
    gotoCommand(name: string, data: BotClasses.AdBoxBot.AdBoxMessage): Promise<{}>;
    setCommand(name: string | RegExp, handler: Function): void;
    setAnswer(name: string | RegExp, handler: Function): void;
    setInvoke(emitter: any, name: string | RegExp, handler: Function): void;
    before(data: any): Promise<{}>;
    emitterEmit(emitter: any, command: any, data: any): Promise<{}>;
}
