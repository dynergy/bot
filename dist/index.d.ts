/// <reference types="node-telegram-bot-api" />
/// <reference types="i18n" />
import * as base2 from './baseController';
import * as TelegramBot from 'node-telegram-bot-api';
import * as moment2 from 'moment-timezone';
export { AdBoxBot } from './BotClasses';
export declare let baseClass: typeof base2.baseController;
export declare let init: (telegramBotKey: string, i18Dir: string, botanKey: string, firebase2: any, proxy?: any) => Promise<{
    botan: any;
    firebase: any;
    moment: typeof moment2;
    bot: TelegramBot;
    botName: string;
    i18n: typeof i18n;
}>;
export { BotInfo } from './bot';
export { commandsPack } from './baseController';
export { redisSave, reditGet } from './red';
